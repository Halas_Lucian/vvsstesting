package inventory.integration.topDown;
import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import inventory.service.ServiceException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RIntegrationTests {
    static InventoryRepository inventoryRepository = new InventoryRepository();
    static InventoryService inventoryService = new InventoryService(inventoryRepository);

    @BeforeEach
    void setup() {
        inventoryRepository.clearParts();
    }

    @AfterEach
    void tearDown() {
        inventoryRepository.clearParts();
    }

    @Test
    @DisplayName("R integration add part successful")
    void R_integration_add_part_successful() {
        InhousePart part = mock(InhousePart.class);
        when(part.getName()).thenReturn("name");
        when(part.getPrice()).thenReturn((double) 10);
        when(part.getInStock()).thenReturn(10);
        when(part.getMin()).thenReturn(10);
        when(part.getMax()).thenReturn(100);

        try {
            inventoryService.addInhousePart(part);
        } catch (ServiceException ex) {
            assert false;
        }

        assertEquals(1, inventoryRepository.getAllParts().size());
    }

    @Test
    @DisplayName("R integration add part unsuccessful")
    void R_integration_add_part_unsuccessful() {
        InhousePart part = mock(InhousePart.class);
        when(part.getName()).thenReturn("");
        when(part.getPrice()).thenReturn((double) 10);
        when(part.getInStock()).thenReturn(10);
        when(part.getMin()).thenReturn(10);
        when(part.getMax()).thenReturn(100);

        Throwable exception = assertThrows(ServiceException.class,
                () -> inventoryService.addInhousePart(part)
        );

        assertEquals("A name has not been entered. A name must have between 2 and 100 characters. ", exception.getMessage());
    }

    @Test
    @DisplayName("R integration lookup successful")
    void R_integration_lookup_successful() throws ServiceException {
        InhousePart part = mock(InhousePart.class, withSettings().useConstructor(1,"name",(double) 10,10,10,100,1));
        when(part.getName()).thenReturn("name");
        when(part.getPrice()).thenReturn((double) 10);
        when(part.getInStock()).thenReturn(10);
        when(part.getMin()).thenReturn(10);
        when(part.getMax()).thenReturn(100);

        inventoryService.addInhousePart(part);
        Part result = inventoryService.lookupPart("name");

        assertEquals(1,inventoryRepository.getAllParts().size());
        assertEquals(part.getName(), result.getName());
    }

    @Test
    @DisplayName("R integration lookup unsuccessful")
    void R_integration_lookup_unsuccessful() {
        Part result = inventoryService.lookupPart("name");
        assertEquals(0,inventoryRepository.getAllParts().size());
        assertNull(result);
    }

}

