package inventory.service;

import inventory.repository.InventoryRepository;
import inventory.service.utils.RandomUtils;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class InventoryServiceTest {

    InventoryRepository inventoryRepository = new InventoryRepository();
    InventoryService inventoryService = new InventoryService(inventoryRepository);

    @BeforeEach
    public void setup() {
    }

    @Tag("ECP")
    @Nested
    @DisplayName("Testing using equivalency classes")
    class EcpTestingInHouseParts {

        @Test
        @DisplayName("ECP - Valid both name and price")
        void Test_addPart_ECP_valid() {
            int check = inventoryRepository.getAllParts().size();
            try {
                inventoryService.addInhousePart("part", 1, 5, 1, 10, 1);
                assertTrue(inventoryRepository.getAllParts().size()==check+1);
            } catch (ServiceException e) {
                assert false;
            }

        }

        @Test
        @DisplayName("ECP - Invalid both name and price")
        void Test_addPart_ECP_nonValid() {


            Throwable exception = assertThrows(ServiceException.class,
                    () ->  inventoryService.addInhousePart("0", 0, 5, 1, 10, 1)
            );
            assertTrue("A name must have between 2 and 100 characters. The price must be greater than 0. ".equals(exception.getMessage()));

        }

        @Test
        @DisplayName("ECP - Invalid name and valid price")
        void Test_addPart_ECP_invalidName_validPrice() {

            Throwable exception = assertThrows(ServiceException.class,
                    () -> inventoryService.addInhousePart("p", 50, 5, 1, 10, 1)
            );
            assertTrue("A name must have between 2 and 100 characters. ".equals(exception.getMessage()));


        }

        @Test
        @DisplayName("Valid name and invalid price")
        void Test_addPart_ECP_validName_invalidPrice() {

            Throwable exception = assertThrows(ServiceException.class,
                    () -> inventoryService.addInhousePart("part", -100, 5, 1, 10, 1)
            );
            assertTrue("The price must be greater than 0. ".equals(exception.getMessage()));


        }
    }

    @Tag("BVA")
    @Nested
    @DisplayName("Testing using border values")
    class BvaTestingInHouseParts {
        @Test
        @DisplayName("Valid both name and price")
        void Test_addPart_BVA_valid() {
            int check = inventoryRepository.getAllParts().size();
            try {
                inventoryService.addInhousePart("pa", 0.01, 5, 1, 10, 1);
                assertTrue(inventoryRepository.getAllParts().size()==check+1);
            } catch (ServiceException e) {
                assert false;
            }
        }

        @Test
        @DisplayName("Invalid both name and price")
        void Test_addPart_BVA_nonvalid_nameTooShort_price() {

            Throwable exception =assertThrows(ServiceException.class,
                    () -> inventoryService.addInhousePart("p", 0, 5, 1, 10, 1)
            );
            assertTrue("A name must have between 2 and 100 characters. The price must be greater than 0. ".equals(exception.getMessage()));


        }

        @Test
        @DisplayName("Invalid name and valid price")
        void Test_addPart_BVA_invalidName_validPrice() {

            Throwable exception =assertThrows(ServiceException.class,
                    () -> inventoryService.addInhousePart("p", 0.01, 5, 1, 10, 1)
            );
            assertTrue("A name must have between 2 and 100 characters. ".equals(exception.getMessage()));


        }

        @Test
        @Timeout(20)
        @DisplayName("Valid name and invalid price")
        void Test_addPart_BVA_validName_invalidPrice() {
            String testName = RandomUtils.RandomString(100);
            Throwable exception =assertThrows(ServiceException.class,
                    () -> inventoryService.addInhousePart(testName, 0, 5, 1, 10, 1)
            );
            assertTrue("The price must be greater than 0. ".equals(exception.getMessage()));

        }
    }
}