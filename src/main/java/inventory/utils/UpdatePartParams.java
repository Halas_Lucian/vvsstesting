package inventory.utils;

public class UpdatePartParams {
    private int partIndex;
    private int partId;
    private String name;
    private double price;
    private int inStock;
    private int min;
    private int max;
    private String partDynamicValue;

    public UpdatePartParams(int partIndex, int partId, String name, double price, int inStock, int min, int max, String partDynamicValue) {
        this.partIndex = partIndex;
        this.partId = partId;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.min = min;
        this.max = max;
        this.partDynamicValue = partDynamicValue;
    }

    public int getPartIndex() {
        return partIndex;
    }

    public void setPartIndex(int partIntex) {
        this.partIndex = partIntex;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getPartDynamicValue() {
        return partDynamicValue;
    }

    public void setPartDynamicValue(String partDynamicValue) {
        this.partDynamicValue = partDynamicValue;
    }
}
