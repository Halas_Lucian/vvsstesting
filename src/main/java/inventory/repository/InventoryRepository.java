package inventory.repository;

import inventory.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.Objects;
import java.util.StringTokenizer;

public class InventoryRepository {

	private static final String filename = "data/items.txt";
	private ProductsInventory productsInventory;
	private final PartsInventory partsInventory;

	public InventoryRepository(){
		this.productsInventory =new ProductsInventory();
		this.partsInventory = new PartsInventory();
//		readParts();
//		readProducts();
	}

	public InventoryRepository(ProductsInventory pr, PartsInventory pa){
		this.productsInventory =pr;
		this.partsInventory = pa;
		//readParts();
		//readProducts();
	}

	public void readParts(){
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		File file = new File(Objects.requireNonNull(classLoader.getResource(filename)).getFile());
		ObservableList<Part> listP = FXCollections.observableArrayList();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=br.readLine())!=null){
				Part part=getPartFromString(line);
				if (part!=null) {
					listP.add(part);
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		partsInventory.setItems(listP);
	}

	private Part getPartFromString(String line){
		if (line==null|| "".equals(line)) {
			return null;
		}
		StringTokenizer st=new StringTokenizer(line, ",");
		String type=st.nextToken();
		Part item=null;
		if ("I".equals(type)) {
			int id= Integer.parseInt(st.nextToken());
			productsInventory.setItemId(id);
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			int idMachine= Integer.parseInt(st.nextToken());
			item = new InhousePart(id, name, price, inStock, minStock, maxStock, idMachine);
		}
		if ("O".equals(type)) {
			int id= Integer.parseInt(st.nextToken());
			productsInventory.setItemId(id);
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			String company=st.nextToken();
			item = new OutsourcedPart(id, name, price, inStock, minStock, maxStock, company);
		}
		return item;
	}

	public void readProducts(){
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		File file = new File(Objects.requireNonNull(classLoader.getResource(filename)).getFile());

		ObservableList<Product> listP = FXCollections.observableArrayList();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=br.readLine())!=null){
				Product product=getProductFromString(line);
				if (product!=null) {
					listP.add(product);
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		productsInventory.setItems(listP);
	}

	private Product getProductFromString(String line){

		if (line==null || "".equals(line)) {
			return null;
		}
		StringTokenizer st=new StringTokenizer(line, ",");
		String type=st.nextToken();
		Product product=null;
		if ("P".equals(type)) {
			int id= Integer.parseInt(st.nextToken());
			partsInventory.setItemId(id);
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			String partIDs=st.nextToken();

			StringTokenizer ids= new StringTokenizer(partIDs,":");
			ObservableList<Part> list= FXCollections.observableArrayList();
			while (ids.hasMoreTokens()) {
				String idP = ids.nextToken();
				Part part = partsInventory.lookup(idP);
				if (part != null) {
					list.add(part);
				}
			}
			product = new Product(id, name, price, inStock, minStock, maxStock, list);
			product.setAssociatedParts(list);
		}
		return product;
	}

	public void writeAll() {

		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		File file = new File(Objects.requireNonNull(classLoader.getResource(filename)).getFile());

		BufferedWriter bw = null;
		ObservableList<Part> parts= partsInventory.getItems();
		ObservableList<Product> products= productsInventory.getItems();

		try {
			bw = new BufferedWriter(new FileWriter(file));
			for (Part p:parts) {
				System.out.println(p.toString());
				bw.write(p.toString());
				bw.newLine();
			}

			for (Product pr:products) {
				StringBuilder line= new StringBuilder(pr.toString() + ",");
				ObservableList<Part> list= pr.getAssociatedParts();
				int index=0;
				while(index<list.size()-1){
					line.append(list.get(index).getPartId()).append(":");
					index++;
				}
				if (index==list.size()-1) {
					line.append(list.get(index).getPartId());
				}
				bw.write(line.toString());
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addPart(Part part){
		partsInventory.add(part);
		writeAll();
	}

	public void addProduct(Product product){
		productsInventory.add(product);
		writeAll();
	}

	public int getAutoPartId(){
		return partsInventory.getItemId();
	}

	public int getAutoProductId(){
		return productsInventory.getItemId();
	}

	public ObservableList<Part> getAllParts(){
		return partsInventory.getItems();
	}

	public ObservableList<Product> getAllProducts(){
		return productsInventory.getItems();
	}

	public Part lookupPart (String search){
		return partsInventory.lookup(search);
	}

	public Product lookupProduct (String search){
		return productsInventory.lookup(search);
	}

	public void updatePart(int partIndex, Part part){
		partsInventory.update(partIndex, part);
		writeAll();
	}

	public void updateProduct(int productIndex, Product product){
		productsInventory.update(productIndex, product);
		writeAll();
	}

	public void deletePart(Part part){
		partsInventory.remove(part);
		writeAll();
	}

	public void deleteProduct(Product product){
		productsInventory.remove(product);
		writeAll();
	}

	public ProductsInventory getInventory(){
		return productsInventory;
	}

	public void setInventory(ProductsInventory productsInventory){
		this.productsInventory = productsInventory;
	}

	public void clearParts() {
		partsInventory.setItems(FXCollections.observableArrayList());
	}

	public void clearProducts() {
		productsInventory.setItems(FXCollections.observableArrayList());
	}
}