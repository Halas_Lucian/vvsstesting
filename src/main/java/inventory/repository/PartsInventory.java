package inventory.repository;

import inventory.model.Part;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PartsInventory implements InventoryInterface<Part>{

    private ObservableList<Part> parts;
    private int autoPartId;

    public PartsInventory(){
        this.parts = FXCollections.observableArrayList();
        this.autoPartId=0;
    }

    public void add(Part part) {
        parts.add(part);
    }

    public void remove(Part part) {
        parts.remove(part);
    }

    public Part lookup(String searchItem) {
        for(Part p:parts) {
            if(p.getName().contains(searchItem) || (p.getPartId() + "").equals(searchItem)) {
                return p;
            }
        }
        return null;
    }

    public void update(int index, Part part) {
        parts.set(index, part);
    }

    public ObservableList<Part> getItems() {
        return parts;
    }

    public void setItems(ObservableList<Part> list) {
        parts=list;
    }

    public int getItemId() {
        autoPartId++;
        return autoPartId;
    }

    public void setItemId(int id){
        autoPartId=id;
    }
}
