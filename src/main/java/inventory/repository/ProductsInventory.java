package inventory.repository;

import inventory.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ProductsInventory implements InventoryInterface<Product> {

    private ObservableList<Product> products;
    private int autoProductId;


    public ProductsInventory(){
        this.products = FXCollections.observableArrayList();
        this.autoProductId=0;
    }

    @Override
    public void add(Product product) {
        products.add(product);
    }

    @Override
    public void remove(Product product) {
        products.remove(product);
    }

    @Override
    public Product lookup(String searchItem) {
        if(searchItem != null && searchItem.length() > 0) {
            for(Product p: products) {
                if(p.getName().contains(searchItem)) {
                    return p;
                } else if ((p.getProductId() + "").equals(searchItem)) {
                    return p;
                }
            }
            return new Product(0, null, 0.0, 0, 0, 0, null);
        } else {
            return new Product(0, null, 0.0, 0, 0, 0, null);
        }
    }

    @Override
    public void update(int index, Product product) {
        products.set(index, product);
    }

    @Override
    public ObservableList<Product> getItems() {
        return products;
    }

    @Override
    public void setItems(ObservableList<Product> list) {
        products = list;
    }

    public int getItemId() {
        autoProductId++;
        return autoProductId;
    }

    public void setItemId(int id){
        autoProductId=id;
    }
    
}