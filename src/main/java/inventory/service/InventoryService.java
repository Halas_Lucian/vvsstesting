package inventory.service;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.utils.UpdatePartParams;
import javafx.collections.ObservableList;

public class InventoryService {

    private final InventoryRepository repo;

    public InventoryService(InventoryRepository repo){
        this.repo = repo;
    }

    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue) throws ServiceException {
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        String msg="";
        msg = Part.isValidPart(inhousePart.getName(),inhousePart.getPrice(),inhousePart.getInStock(),inhousePart.getMin(),inhousePart.getMax(),msg);

        if (msg.isEmpty()) {
            repo.addPart(inhousePart);
        } else {
            throw new ServiceException(msg);
        }
    }

    public void addInhousePart(InhousePart inhousePart) throws ServiceException {
        inhousePart.setPartId(repo.getAutoPartId());
        String msg="";
        msg = Part.isValidPart(inhousePart.getName(),inhousePart.getPrice(),inhousePart.getInStock(),inhousePart.getMin(),inhousePart.getMax(),msg);

        if (msg.isEmpty()) {
            repo.addPart(inhousePart);
        } else {
            throw new ServiceException(msg);
        }
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        repo.addPart(outsourcedPart);
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts){
        Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
        repo.addProduct(product);
    }

    public ObservableList<Part> getAllParts() {
        return repo.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    public Part lookupPart(String search) {
        return repo.lookupPart(search);
    }

    public Product lookupProduct(String search) {
        return repo.lookupProduct(search);
    }

    public void updateInhousePart(UpdatePartParams params){
        InhousePart inhousePart = new InhousePart(params.getPartId(), params.getName(), params.getPrice(),
                params.getInStock(), params.getMin(), params.getMax(), Integer.parseInt(params.getPartDynamicValue()));
        repo.updatePart(params.getPartIndex(), inhousePart);
    }

    public void updateOutsourcedPart(UpdatePartParams params){
        OutsourcedPart outsourcedPart = new OutsourcedPart(params.getPartId(), params.getName(), params.getPrice(),
                params.getInStock(), params.getMin(), params.getMax(), params.getPartDynamicValue());
        repo.updatePart(params.getPartIndex(), outsourcedPart);
    }

    public void updateProduct(int productIndex, int productId, String name, double price, int inStock, int min, int max, ObservableList<Part> addParts){
        Product product = new Product(productId, name, price, inStock, min, max, addParts);
        repo.updateProduct(productIndex, product);
    }

    public void deletePart(Part part){
        repo.deletePart(part);
    }

    public void deleteProduct(Product product){
        repo.deleteProduct(product);
    }

}
